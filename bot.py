import telebot
import config
import db

bot = telebot.TeleBot('878252372:AAEQeb19eyIEWwM2gD0NHCraRHVzVCL1uZU')


@bot.message_handler(commands=["start"])
def cmd_start(message):
    bot.send_message(message.chat.id, "Привет! я могу отличить кота от хлеба! объект перед тобой квадратный?")
    db.set_state(message.chat.id, config.States.S_UZEL1.value)


L = ['ага', 'конечно', 'пожалуй']
M = ['нет, конечно', 'ноуп', 'найн']


@bot.message_handler(func=lambda message: db.get_current_state(message.chat.id) == config.States.S_UZEL1.value)
def user_entering_mes(message):
    z = message.text.lower()
    if z in L:
        bot.send_message(message.chat.id, "У него есть уши?")
    if z in M:
        bot.send_message(message.chat.id, "Это кот, а не хлеб! Не ешь его!")
    db.set_state(message.chat.id, config.States.S_UZEL2.value)


@bot.message_handler(func=lambda message: db.get_current_state(message.chat.id) == config.States.S_UZEL2.value)
def user_entering(message):
    x = message.text.lower()
    if x in L:
        bot.send_message(message.chat.id, "Это кот, а не хлеб! Не ешь его!")
    if x in M:
        bot.send_message(message.chat.id, "Это хлеб, а не кот! Ешь его!")

    db.set_state(message.chat.id, config.States.S_UZEL3.value)


bot.polling(none_stop=True, interval=0)
